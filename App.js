import React, { useEffect, useState } from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Amplify, { Analytics } from 'aws-amplify';
import config from './aws-exports';
import StartScreen from './src/screens/StartScreen';
import MySpacesListScreen from './src/screens/MySpacesListScreen';
import SpaceDetailScreen from './src/screens/SpaceDetailScreen';
import SpaceDetailScreen1 from './src/screens/SpaceDetailScreen1';
import OnBoardingScreens from './src/screens/OnBoardingScreens';
import ScrollableHeader from './src/screens/ScrollableHeader';

Amplify.configure(config);

const navigator = createStackNavigator({
  // Start: StartScreen,
  MySpacesList: MySpacesListScreen,
  SpaceDetail: SpaceDetailScreen,
  SpaceDetail1: SpaceDetailScreen1,
  Test: ScrollableHeader
}, {
    initialRouteName: 'MySpacesList',
    // initialRouteName: 'Test',
    defaultNavigationOptions: {
      // title: 'My Spaces'
    }
  });

const App = createAppContainer(navigator);

export default () => {
  const [showRealApp, setShowRealApp] = useState(false);

  useEffect(() => {
    Analytics.record('User opened Application');
    return () => {
      Analytics.record('User closed Application')
    };
  }, [])

  return (
    showRealApp
      ? <App />
      : <OnBoardingScreens toggleAppVisibility={setShowRealApp} />
  );
};
