import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

const RadioButton = ({
  value,
  isSelected = false,
  onChange,
  containerStyle,
  children,
  backgroundColor,
  overlapColor,
  selectedColor,
  overlapWidth,
  disabled
}) => {
  return (
    <TouchableOpacity
      style={[styles.radio, containerStyle, { position: 'relative' }]}
      onPress={() => {
        if (!disabled) onChange(value)
      }}
    >
    <View style={{
        flexDirection: 'row',
        width: overlapWidth,
        // width: '100%',
        backgroundColor: isSelected ? overlapColor : '#fff',
        position: 'absolute',
        height: 38,
        // top: 0.5,
        left: 0
      }}></View>
        <Icon
          size={20}
          color={isSelected ? selectedColor : backgroundColor}
          name='circle'
          style={{
            marginTop: 10
          }}
        />
        {
          children
        }

    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  radio: {
    justifyContent: 'center',
    flexDirection: 'row'
  },
  radioOverlap: {
    flexDirection: 'row',
    alignItems: 'center'
  }
});

export default RadioButton;