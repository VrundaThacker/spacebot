
import React, { useState } from 'react';
import { View, StyleSheet, Text, ScrollView } from 'react-native';
import { WebView } from 'react-native-webview';
import ModalBox from 'react-native-modalbox';

const AppFeedback = ({ open, toggleModalVisibility }) => {
  return (
    <ModalBox
      isOpen={open}
      style={styles.modal}
      position='bottom'
      entry='bottom'
      swipeToClose={false}
      onClosed={() => toggleModalVisibility(false)}
    >
      {/* <WebView source={{ uri: 'https://docs.google.com/forms/d/e/1FAIpQLSf2EgK4Ham58BmJRuBmfh4-9kxqnLAnkBF9iaQ0xabKAVhLug/viewform?embedded=true' }} /> */}
      <ScrollView
        contentContainerStyle={{ flexGrow: 1, overflow: 'scroll' }}
      >
        <WebView
          source={{ uri: 'https://docs.google.com/forms/d/e/1FAIpQLSf2EgK4Ham58BmJRuBmfh4-9kxqnLAnkBF9iaQ0xabKAVhLug/viewform?embedded=true' }}
          style={{ height: 300, overflow: 'scroll' }} />
      </ScrollView>

    </ModalBox>
  );
};

const styles = StyleSheet.create({
  modal: {
    height: '80%',
    backgroundColor: '#fff',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    overflow: 'scroll'
  }
});

export default AppFeedback;



