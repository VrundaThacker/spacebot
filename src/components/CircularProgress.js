import React from 'react';
import {View, StyleSheet} from 'react-native';

const propStyle = (percent, base_degrees) => {
  const rotateBy = base_degrees + (percent * 3.6);
  return {
    transform:[{rotateZ: `${rotateBy}deg`}]
  };
}

const renderThirdLayer = (percent) => {
  if(percent > 50){
    /**
    * Third layer circle default is 45 degrees, so by default it occupies the right half semicircle.
    * Since first 50 percent is already taken care  by second layer circle, hence we subtract it
    * before passing to the propStyle function
    **/
    return <View style={[styles.secondProgressLayer,propStyle((percent - 50), 45) ]}></View>
  }else{
    return <View style={styles.offsetLayer}></View>
  }
}

const CircularProgress = ({percent}) => {
  let firstProgressLayerStyle;
  if(percent > 50){
      firstProgressLayerStyle = propStyle(50, -135);
  }else {
    firstProgressLayerStyle = propStyle(percent, -135);
  }

  return(
    <View style={styles.container}>
      <View style={[styles.firstProgressLayer, firstProgressLayerStyle]}></View>
      {renderThirdLayer(percent)}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: 30,
    height: 30,
    borderWidth: 2,
    borderRadius: 15,
    borderColor: '#46A9F1',
    justifyContent: 'center',
    alignItems: 'center'
  },
  firstProgressLayer: {
    width: 30,
    height: 30,
    borderWidth: 2,
    borderRadius: 15,
    position: 'absolute',
    borderLeftColor: 'transparent',
    borderBottomColor: 'transparent',
    borderRightColor: '#fff',
    borderTopColor: '#fff',
    transform:[{rotateZ: '-135deg'}]
  },
  secondProgressLayer:{
    width: 30,
    height: 30,
    position: 'absolute',
    borderWidth: 2,
    borderRadius: 15,
    borderLeftColor: 'transparent',
    borderBottomColor: 'transparent',
    borderRightColor: '#fff',
    borderTopColor: '#fff',
    transform: [{rotateZ: '45deg'}]
  },
  offsetLayer: {
    width: 30,
    height: 30,
    position: 'absolute',
    borderWidth: 2,
    borderRadius: 15,
    borderLeftColor: 'transparent',
    borderBottomColor: 'transparent',
    borderRightColor: '#46A9F1',
    borderTopColor: '#46A9F1',
    transform:[{rotateZ: '-135deg'}]
  }
});

export default CircularProgress;