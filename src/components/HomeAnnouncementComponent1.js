import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Platform, TouchableHighlight } from 'react-native';
import { Icon } from 'native-base'
import { Analytics } from 'aws-amplify';
import { LinearGradient } from 'expo-linear-gradient';


const HomeAnnouncementComponent = ({ details, navigateToBroadcasts, selectedTab, toggleVisibility }) => {
  return <TouchableHighlight
    style={styles.container}
    onPress={() => {
      // selectedTab === 1 ? toggleVisibility(false) : navigateToBroadcasts(1);
      if (selectedTab === 1) {
        toggleVisibility(false);
        Analytics.record('User closed HomeAnnouncementWidget');
      } else {
        navigateToBroadcasts(1);
        Analytics.record('User Navigated to Broadcast screen via HomeAnnouncementWidget');
      }
    }}
    >
    <LinearGradient
      colors={['#46a9f1', '#1380d0']}
      start={[1, 0]} end={[0, 1]}
      style={{ 
        padding: 10,
        alignItems: 'center',
        flexDirection: 'row',
        height: 40,
      }}>

      <View style={{
        width: 0,
        flexGrow: 1,
        flex: 1,
      }}>
        <Text style={{
          backgroundColor: 'transparent',
          fontSize: 12,
          color: '#fff',
          flex: 1, flexWrap: 'wrap'
        }}>
          {
            ((details.title).length > 50) ?
              (((details.title).substring(0, 50 - 3)) + '...') :
              details.title
          }
        </Text>
      </View>
      <View style={{
        alignSelf: 'flex-end',
        backgroundColor: '#1582D2',
        width: 18,
        height: 18,
        borderRadius: 20,
        position: 'absolute',
        top: 10,
        right: 10,
      }}
      >
        <Icon
          type='MaterialCommunityIcons'
          name={selectedTab === 1 ? 'close' : 'chevron-right'}
          style={{
            color: '#46A9F1',
            // alignSelf: 'center',
            fontSize: 16,
            padding: 1
          }}
        />
      </View>
    </LinearGradient>
  </TouchableHighlight>
};

const styles = StyleSheet.create({
  container: {
    width: '100%'
  }
});

export default HomeAnnouncementComponent;