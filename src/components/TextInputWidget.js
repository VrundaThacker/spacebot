import React, { useState } from 'react';
import { View, StyleSheet, TextInput, TouchableOpacity, Image, Text } from 'react-native';
import CheckBox from 'react-native-check-box';
import { Analytics } from 'aws-amplify';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Divider from './Divider';

const ratings = [
  {
    text: 'Miserable',
    uri: require('../../assets/miserable.png')
  },
  {
    text: 'CanDoBetter',
    uri: require('../../assets/couldBeBetter.png')
  },
  {
    text: 'Good',
    uri: require('../../assets/good.png')
  },
  {
    text: 'Awesome',
    uri: require('../../assets/awesome.png')
  },
  {
    text: 'Marvellous',
    uri: require('../../assets/marvellous.png')
  }
];

const TextInputWidget = ({ handleFeedback }) => {
  const [selectedRating, setSelectedRating] = useState([false, false, false, false, false]);
  const [userFeedback, setUserFeedback] = useState('');
  const [showRatingEmoticons, setVisbilityRatingEmoticons] = useState(true);

  const isRatingSelected = () => selectedRating.find(rate => rate === true);

  const getRatingText = () => {
    const selectedRatingIndex = selectedRating.findIndex(rate => rate === true);
    return ratings[selectedRatingIndex];
  }

  const updateSelectedRating = (index) => {
    const newArr = [...selectedRating];
    selectedRating.map((rating, i) => {
      if (index === i) {
        return newArr[i] = !newArr[i];
      } else {
        return newArr[i] = false;
      }
    });
    setSelectedRating(newArr);
  }

  return <View style={{
    // flex: 1
  }}>
    {
      showRatingEmoticons
        ? <View style={{
          flexDirection: 'row',
          height: 100
        }}>
          <CheckBox
            isChecked={selectedRating[0]}
            unCheckedImage={<Image source={require('../../assets/miserable.png')} style={{ height: 40, width: 40 }} />}
            checkedImage={<View>
              <Image source={require('../../assets/miserable.png')} style={{ height: 70, width: 70 }} />
              <Text style={styles.iconText}>Miserable!</Text>
            </View>}
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center'
            }}
            onClick={() => updateSelectedRating(0)}
          />
          <CheckBox
            isChecked={selectedRating[1]}
            unCheckedImage={<Image source={require('../../assets/couldBeBetter.png')} style={{ height: 40, width: 40 }} />}
            checkedImage={<View>
              <Image source={require('../../assets/couldBeBetter.png')} style={{ height: 70, width: 70 }} />
              <Text style={styles.iconText}>Poor!</Text>
            </View>}
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center'
            }}
            onClick={() => updateSelectedRating(1)}
          />
          <CheckBox
            isChecked={selectedRating[2]}
            unCheckedImage={<Image source={require('../../assets/good.png')} style={{ height: 40, width: 40 }} />}
            checkedImage={<View>
              <Image source={require('../../assets/good.png')} style={{ height: 70, width: 70 }} />
              <Text style={styles.iconText}>Good!</Text>
            </View>}
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center'
            }}
            onClick={() => updateSelectedRating(2)}
          />
          <CheckBox
            isChecked={selectedRating[3]}
            unCheckedImage={<Image source={require('../../assets/awesome.png')} style={{ height: 40, width: 40 }} />}
            checkedImage={<View>
              <Image source={require('../../assets/awesome.png')} style={{ height: 70, width: 70 }} />
              <Text style={styles.iconText}>Awesome!</Text>
            </View>}
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center'
            }}
            onClick={() => updateSelectedRating(3)}
          />
          <CheckBox
            isChecked={selectedRating[4]}
            unCheckedImage={<Image source={require('../../assets/marvellous.png')} style={{ height: 40, width: 40 }} />}
            checkedImage={<View>
              <Image source={require('../../assets/marvellous.png')} style={{ height: 70, width: 70 }} />
              <Text style={styles.iconText}>Marvellous!</Text>
            </View>}
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center'
            }}
            onClick={() => updateSelectedRating(4)}
          />
        </View>
        : null
    }
    <Divider />
    <View style={{
      flexDirection: 'row',
      justifyContent: 'center',
      paddingHorizontal: 14
    }}>
      <TouchableOpacity
        style={{
          alignSelf: 'center'
        }}
        onPress={() => setVisbilityRatingEmoticons(!showRatingEmoticons)}
      >
        <Icon
          size={28}
          name='emoticon-happy-outline'
          color={showRatingEmoticons ? '#1DBDA0' : '#C4C4C4'}
        />
      </TouchableOpacity>
      <TextInput
        style={{
          fontSize: 16,
          marginBottom: 15,
          margin: 10,
          padding: 5,
          backgroundColor: 'rgba(248, 248, 248, 0.61)',
          width: '85%',
          borderRadius: 14,
        }}
        value={userFeedback}
        onChangeText={setUserFeedback}
        keyboardType='default'
        placeholder={'Give feedback directly to owner?'}
        placeholderTextColor={'#dadada'}
      />
      <TouchableOpacity
        disabled={!userFeedback && !isRatingSelected()}
        onPress={() => {
          handleFeedback(getRatingText(), userFeedback);
          Analytics.record('User submitted Space Feedback');
          if(showRatingEmoticons) setVisbilityRatingEmoticons(false);
          if (userFeedback) setUserFeedback('')
          if (selectedRating) setSelectedRating([false, false, false, false, false])
        }}
      >
        <View style={{
          backgroundColor: !userFeedback && !isRatingSelected() ? '#F7F7F7' : '#E2F6F3',
          borderRadius: 50,
          height: 30,
          width: 30,
          marginTop: 10
        }}>
          <Icon
            size={20}
            color={!userFeedback && !isRatingSelected() ? '#DADADA' : '#1DBDA0'}
            name='arrow-right'
            style={{
              margin: 5,
            }}
          />
        </View>
      </TouchableOpacity>
    </View>
  </View>
};

const styles = StyleSheet.create({
});

export default TextInputWidget;