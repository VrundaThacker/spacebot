import React from 'react';
import { View, StyleSheet, Image } from 'react-native';

const CustomImage = ({ imageName }) => {
  return <Image style={styles.image} source={imageName} />
};

const styles = StyleSheet.create({
  image: {
    height: 20,
    width: 20,
    marginLeft: 5
  }
});

export default CustomImage;