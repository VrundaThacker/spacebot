import React from 'react'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { TouchableOpacity, Text, StyleSheet, View } from 'react-native'

const CheckBox = ({
  selected,
  onPress,
  style,
  textStyle,
  color = '#211f30',
  children, ...props }) => (
    // <TouchableOpacity style={[styles.checkBox, style]} onPress={onPress} {...props}>
    //     <Icon
    //         size={20}
    //         color={selected ? '#FAAF1D': '#FEF1D9'}
    //         name='circle'
    //     />

    //     {children}
    // </TouchableOpacity>

    <TouchableOpacity style={
      [styles.checkBox, style]
    }>
        <View style={{
          backgroundColor: '#FAAF1D',
          height: 30,
          borderRadius: 10,
          width: 20
        }}>
        <Icon
          size={20}
          color={selected ? '#FAAF1D' : '#FEF1D9'}
          name='circle'
        />
      </View>
    </TouchableOpacity>
  )

const styles = StyleSheet.create({
  checkBox: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: 5,
    backgroundColor: '#FEF1D9',
    height: 30,
    borderRadius: 10,
    marginHorizontal: 5,
    position: 'relative'
  }
})

export default CheckBox