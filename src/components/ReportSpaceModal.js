import React, { useState } from 'react';
import { View, StyleSheet, Text, TextInput } from 'react-native';
import { Button } from 'native-base';
import { Analytics } from 'aws-amplify';
import ModalBox from 'react-native-modalbox';
import Spacer from './Spacer';


const ReportSpaceModal = ({ open, toggleModalVisibility }) => {
  const [userInput, setUserInput] = useState('');
  return (
    <ModalBox
      isOpen={open}
      style={styles.modal}
      position='bottom'
      entry='bottom'
      onClosed={() => toggleModalVisibility(false)}
    >
      <View style={styles.container}>
        <Text style={{
          fontWeight: 'bold',
          fontSize: 16
        }}>Report</Text>
        <Spacer />
        <Text>Tell us what is wrong with this space</Text>
        <TextInput
          style={{
            fontSize: 14,
            marginBottom: 15,
            marginVertical: 10,
            padding: 5,
            borderWidth: 1,
            borderColor: '#e8e8e8',
            width: '90%',
            borderRadius: 4,
          }}
          value={userInput}
          onChangeText={setUserInput}
          keyboardType='default'
          multiline={true}
          placeholder={'Type here...'}
          placeholderTextColor={'#dadada'}
        />
        <Button disabled={!userInput} block
          style={{ backgroundColor: !userInput ? '#efefef' : '#1DBDA0', width: 100, borderRadius: 20, alignSelf: 'center' }}
          onPress={() => {
            toggleModalVisibility(false);
            Analytics.record('User submitted Space Feedback');
          }}>
          <Text style={{ color: !userInput ? '#ccc' : '#fff' }}>Submit</Text>
        </Button>
      </View>

    </ModalBox>
  );
};

const styles = StyleSheet.create({
  modal: {
    height: 230,
    backgroundColor: '#fff',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20
  },
  container: {
    marginLeft: 15,
    marginTop: 20
  }
});

export default ReportSpaceModal;