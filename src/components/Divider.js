import React from 'react';
import { View, StyleSheet } from 'react-native';

const Divider = ({ color, width }) => {
  return <View
    style={{
      // borderBottomColor: color,
      // borderBottomWidth: width,
      borderWidth: 0.25,
      borderColor: 'rgba(128, 128, 128, 0.25)',
      borderBottomWidth: 0,
      shadowColor: '#000',
      shadowOffset: { width: 0, height: -2 },
      shadowOpacity: 0.9,
      shadowRadius: 2,
      elevation: 1,
      marginTop: 5,
      width: '100%'
    }}
  />
};

export default Divider;