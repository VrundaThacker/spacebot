import React from 'react';
import { View, StyleSheet } from 'react-native';

const ProgressBar = ({ height, primaryColor, overlapColor, borderRadius, overlapWidth }) => {
  return <View style={{
    backgroundColor: primaryColor,
    height,
    borderRadius,
    marginHorizontal: 5,
    position: 'relative'
  }}>
    <View style={{
      backgroundColor: overlapColor,
      height,
      borderRadius,
      // position: 'absolute'
      width: overlapWidth
    }}></View>
  </View>
};

const styles = StyleSheet.create({

});

export default ProgressBar;