import React, { useEffect, useState, useRef } from 'react';
import { View, StyleSheet, Text, ScrollView } from 'react-native';
import Spacer from './Spacer';
import CustomImage from './CustomImage';

const today = new Date();

const DummyChatWidget = ({ feedback }) => {
  const myScrollView = useRef();
  const [showChatTypingStatus, setChatTypingStatus] = useState(true);

  const firstFeedback = feedback[0]
  const newFeedback = [...feedback];
  newFeedback.shift();
  useEffect(() => {
    const timeoutHandle = setTimeout(() => {
      setChatTypingStatus(false);
    }, 1000);
  }, [])

  const renderOwnerResponse = () => {
    if (showChatTypingStatus) {
      return <View style={{ backgroundColor: '#F7F7F7', borderRadius: 10, width: 40, paddingTop: 10, paddingHorizontal: 5, alignItems: 'center' }}>
        <Text style={{ color: '#c4c4c4', fontSize: 32, lineHeight: 14 }}>...</Text>
      </View>;
    } else {
      return <View>
        <View style={{ backgroundColor: '#c4c4c4', height: 30, width: 220, borderTopRightRadius: 8, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'baseline'}}>
          <Text style={{ color: '#fff', padding: 5, fontWeight: 'bold', fontSize: 14 }}>Owner</Text>
          <Text style={{ color: '#fff', padding: 5, fontSize: 12 }}>{`${today.getHours()}:${today.getMinutes()}`}</Text>
        </View>
        <View style={{
          backgroundColor: '#f7f7f7',
          height: 70, width: 220,
          alignSelf: 'flex-start',
          borderBottomLeftRadius: 8,
          borderBottomRightRadius: 8,
          padding: 10
        }}>
          <Text style={{ color: 'rgba(2, 28, 47, 0.71)' }}>Thank you for your feedback!</Text>
          <Text style={{ color: 'rgba(2, 28, 47, 0.71)' }}>We will get in touch with you shortly.</Text>
        </View>
      </View>
    }
  }

  return (
    <ScrollView
      ref={myScrollView}
      inputRef={myScrollView}
      style={{ margin: 10, flex: 1 }}
      onContentSizeChange={(contentWidth, contentHeight) => {
        myScrollView.current.scrollToEnd({ animated: true });
      }}>
      
      <View style={{
        width: 300,
        alignSelf: 'flex-end',
        backgroundColor: '#1DBDA0',
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8
      }}>

        <View style={{ backgroundColor: '#169E86', height: 30, borderTopLeftRadius: 8, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'baseline'}}>
          <Text style={{ color: '#fff', padding: 5, fontWeight: 'bold', fontSize: 14 }}>Feedback</Text>
          <Text style={{ color: '#fff', padding: 5, fontSize: 12 }}>{`${today.getHours()}:${today.getMinutes()}`}</Text>
        </View>
        <View>
          {
            firstFeedback.rating
              ? <View style={{ backgroundColor: '#fff', margin: 6, padding: 4, borderRadius: 4, flexDirection: 'row' }}>
                <CustomImage fromWeb={false} imageName={firstFeedback.rating.uri} />
                <Text style={{ marginLeft: 15 }}>{`${firstFeedback.rating.text}!`}</Text>
              </View>
              : null
          }
          <Text style={{ margin: 6, color: '#fff' }}>{firstFeedback.userInput}</Text>
        </View>
      </View>
      <Spacer />
      {
        renderOwnerResponse()
      }
      {
        newFeedback.length
          ? newFeedback.map((fb, i) => {

            return (
              <View style={{
                width: 300,
                alignSelf: 'flex-end',
                backgroundColor: '#1DBDA0',
                borderTopRightRadius: 8,
                borderTopLeftRadius: 8,
                borderBottomLeftRadius: 8,
                borderBottomRightRadius: 8,
                marginVertical: 10
              }} key={i}>
                <View style={{ backgroundColor: '#169E86', height: 30, borderTopLeftRadius: 8, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'baseline'}}>
                  <Text style={{ color: '#fff', padding: 5, fontWeight: 'bold', fontSize: 14 }}>You</Text>
                  <Text style={{ color: '#fff', padding: 5, fontSize: 12 }}>{`${today.getHours()}:${today.getMinutes()}`}</Text>
                </View>

                <View>
                  {
                    fb.rating
                      ? <View style={{ backgroundColor: '#fff', margin: 6, padding: 4, borderRadius: 4, flexDirection: 'row' }}>
                        <CustomImage fromWeb={false} imageName={fb.rating.uri} />
                        <Text style={{ marginLeft: 15 }}>{`${fb.rating.text}!`}</Text>
                      </View>
                      : null
                  }
                  <Text style={{ margin: 6, color: '#fff' }}>{fb.userInput}</Text>
                </View>
              </View>
            )

          })
          : null
      }
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  spacer: {
    margin: 10
  }
});

export default DummyChatWidget;