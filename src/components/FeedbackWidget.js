import React, { useState } from 'react';

import { View, StyleSheet, Text, Image, TextInput, TouchableOpacity, KeyboardAvoidingView } from 'react-native';
import Divider from './Divider';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const FeedbackWidget = ({ handleFeedback }) => {
  return (
    // <KeyboardAwareScrollView contentContainerStyle={{
    //   flex: 1,
    //   justifyContent: 'flex-end',
    //   extraScrollHeight: 200,
    //   extraHeight: 200
    // }}
    //   scrollEnabled={true}
    //   enableAutomaticScroll={true}>

      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'flex-end'
        }}
      >
        <Text style={{
          fontWeight: 'bold',
          fontSize: 16,
          alignContent: 'center'
        }}>How was your experience with us?</Text>
        <Text style={{
          color: '#C4C4C4',
          fontSize: 12
        }}>We'd love to hear your feedback!</Text>
      </View>
    // </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  image: {
    width: 50,
    height: 50
  },
  iconText: {
    fontSize: 12,
    alignSelf: 'center'
  }
});

export default FeedbackWidget;