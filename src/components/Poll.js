import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import RadioButton from './RadioButton'

const renderPollOption = (option, backgroundColor,
  overlapColor,
  selectedColor,
  textColor,
  onChange, isSelectedKey) => {
  return <RadioButton
    key={option.key}
    value={option.key}
    isSelected={isSelectedKey === option.key}
    // disabled={isSelectedKey}
    onChange={onChange}
    containerStyle={{
      height: 40,
      borderRadius: 5,
      marginVertical: 5,
      marginHorizontal: 20,
      backgroundColor: option.isSelected ? backgroundColor : '#fff',
      borderColor: 'rgba(174, 174, 174, 0.25)',
      borderWidth: 1
    }}
    backgroundColor={backgroundColor}
    overlapColor={overlapColor}
    selectedColor={selectedColor}
    overlapWidth={option.percentage}
  >
    <View style={{
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      flex: 1,
      marginHorizontal: 5,
    }}>
      <Text style={{ fontSize: 10, color: option.isSelected ? selectedColor : textColor }}>{option.text}</Text>
      <Text style={{ fontSize: 10, color: option.isSelected ? selectedColor : textColor }}>{option.percentage}</Text>
    </View>
  </RadioButton>
}

const Poll = ({ question,
  options, backgroundColor, overlapColor,
  selectedColor, textColor, onChange, isSelectedKey }) => {
  return <View>
    <Text style={{
      fontWeight: 'bold',
      marginLeft: 15,
      fontSize: 16
    }}>
      {question}
    </Text>
    {
      options.map(option => {
        return renderPollOption(option, backgroundColor,
          overlapColor,
          selectedColor,
          textColor,
          onChange, isSelectedKey)
      })
    }
  </View>
};

export default Poll;