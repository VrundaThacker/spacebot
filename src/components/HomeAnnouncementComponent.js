import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import CircularProgress from './CircularProgress';

const HomeAnnouncementComponent = ({ details, navigateToBroadcasts }) => {
  return <View style={{
    width: '94%',
    alignSelf: 'center',
    position: 'absolute',
    top: 140
  }}>
    <LinearGradient
      colors={['#46a9f1', '#1380d0']}
      start={[1, 0]} end={[0, 1]}
      style={{ padding: 15, alignItems: 'center', borderRadius: 5, flexDirection: 'row' }}>
      <View style={{width: '10%'}}>
        <CircularProgress percent={60} />
        <Text style={{
          backgroundColor: 'transparent',
          fontSize: 8,
          color: '#BBDAF1',
          alignSelf: 'flex-start',
          marginTop: 5,
        }}>3 hours left</Text>
      </View>

      <View style={{marginLeft: 20,width: 0,
        flexGrow: 1,
        flex: 1,}}>
        <Text
          style={{
            backgroundColor: 'transparent',
            fontSize: 12,
            color: '#fff',
            flex: 1, flexWrap: 'wrap'
          }}>
          {details.title}
        </Text>
        <Text style={{
          backgroundColor: 'transparent',
          fontSize: 8,
          color: '#BBDAF1',
          alignSelf: 'flex-start',
        }}>{details.description}</Text>
      </View>
      <TouchableOpacity style={{
        alignSelf: 'flex-end',
        backgroundColor: '#1582D2',
        width: 20,
        height: 20,
        borderRadius: 20,
        position: 'absolute',
        top: 30,
        right: 5

      }} 
      onPress={() => { console.log('home annoucement press'); navigateToBroadcasts(1); }}
      >
        <Text style={{
          color: '#46A9F1',
          alignSelf: 'center'
        }}>></Text>
      </TouchableOpacity>
    </LinearGradient>
  </View>
};

const styles = StyleSheet.create({
  spacer: {
    margin: 10
  }
});

export default HomeAnnouncementComponent;