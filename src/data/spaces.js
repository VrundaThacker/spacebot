export const mySpacesList = [
  {
    id: '1',
    title: 'Jeavio Ltd',
    description: 'We are a team of entrepreneurs with a rich experience of the domain, expertise to understand the business and a drive to see things through.',
    phoneNumber: '+918971546603',
    industry: 'Corporate Office',
    email: 'abc@xyz.com',
    address: 'Sanket Heights, Sun Pharma Rd, Parvati Nagar, Tandalja, Vadodara, Gujarat 390012',
    imgUrl: 'https://s3-media2.fl.yelpcdn.com/bphoto/MbKsfg5P1MwYfTajSe1kPA/o.jpg',
    rating: 4.3,
    latLong: {},
    broadcasts: [{
      type: 'announcement',
      title: 'We are having a giveaway for the first 5 to reach cafeteria!',
      description: 'Hurry up!!.'
    }, {
      type: 'rating',
      title: 'Rate the Republic day event held in the last month'
    }, {
      type: 'poll',
      title: 'Where would you like to go for team lunch on 5th March',
      options: [
        {
          text: 'Sayaji',
          key: 'Sayaji',
          percentage: '43%'
        }, {
          text: 'Indish Lounge',
          key: 'Indish Lounge',
          percentage: '12%'
        }, {
          text: 'Surya Palace',
          key: 'Surya Palace',
          percentage: '27%'
        }, {
          text: 'Royal Orchid',
          key: 'Royal Orchid',
          percentage: '18%'
        }
      ]
    }, {
      type: 'alert',
      title: 'Assemble at the gate for Fire drill!'
    }]
  }, {
    id: '2',
    title: 'Hebe Style Studio',
    description: 'Behind every beautiful women is a hairdresser that loves her!',
    address: 'Sanket Heights, Sun Pharma Rd, Parvati Nagar, Tandalja, Vadodara, Gujarat 390012',
    phoneNumber: '+910000000000',
    industry: 'Grooming',
    email: 'abc@xyz.com',
    imgUrl: 'https://s3-media2.fl.yelpcdn.com/bphoto/05z2AVbVp9tOrx57SbQ1VA/o.jpg',
    rating: 2,
    latLong: {},
    broadcasts: [{
      type: 'announcement',
      title: 'We having a free hair makeover session for first 10 visitors',
      description: 'Give yourself a new look!'
    }]
  }, {
    id: '3',
    title: 'United Kitchens',
    description: 'Introducing Vadodara\'s first premium food pak, located at Sun Pharma road on a grand 10,000+ sqft area with 24+ unique restaurants and cafe brands',
    phoneNumber: '+910000000000',
    email: 'abc@xyz.com',
    industry: 'Food & Beverages',
    address: 'Sanket Heights, Sun Pharma Rd, Parvati Nagar, Tandalja, Vadodara, Gujarat 390012',
    imgUrl: 'https://s3-media4.fl.yelpcdn.com/bphoto/cSDgVuPMnJgMLTrTNSEXug/o.jpg',
    rating: 2.5,
    latLong: {},
    broadcasts: [{
      type: 'announcement',
      title: 'Chinese checkers is having live music today at 8pm!',
    }, {
      type: 'rating',
      title: 'We would like to know how was the christmas themed event last month?'
    }]
  }, {
    id: '4',
    title: 'Reliance Mega Mall',
    email: 'abc@xyz.com',
    industry: 'Shopping Hub',
    address: 'Sanket Heights, Sun Pharma Rd, Parvati Nagar, Tandalja, Vadodara, Gujarat 390012',
    imgUrl: 'https://s3-media1.fl.yelpcdn.com/bphoto/gLBxGuuS6KQ9LUYpGcR5Tw/o.jpg',
    description: '',
    rating: 4,
    latLong: {},
    broadcasts: [{
      type: 'rating',
      title: 'How was the children super singing event'
    }, {
      type: 'poll',
      title: 'What cuisine restaurant would you like to have in the mall?',
      options: [{
        text: 'Soda Shop',
        key: 'Soda Shop',
        percentage: '0%'
      }, {
        text: 'Mumbai street food',
        key: 'Mumbai street food',
        percentage: '62%'
      }, {
        text: 'Thai',
        key: 'Thai',
        percentage: '38%'
      }]
    },  {
      type: 'alert',
      title: 'Escalators under maintenance, please use lifts or stairs!'
    }]
  }, {
    id: '5',
    title: 'Burger King',
    address: 'Sanket Heights, Sun Pharma Rd, Parvati Nagar, Tandalja, Vadodara, Gujarat 390012',
    imgUrl: 'https://s3-media3.fl.yelpcdn.com/bphoto/1rHd7F4zztAsScwzM0vDmw/o.jpg',
    description: 'The best burgers you have ever tasted!',
    industry: 'Food & Beverages',
    rating: 3.3,
    email: 'abc@xyz.com',
    latLong: {},
    broadcasts: [{
      type: 'announcement',
      title: 'Please collect our anniversary special gift at the billing counter',
    }]
  }, {
    id: '6',
    title: 'Pizza Planet',
    phoneNumber: '+910000000000',
    email: 'abc@xyz.com',
    industry: 'Food & Beverages',
    description: 'Happiness comes in 6 slices',
    address: 'Sanket Heights, Sun Pharma Rd, Parvati Nagar, Tandalja, Vadodara, Gujarat 390012',
    imgUrl: 'https://s3-media2.fl.yelpcdn.com/bphoto/clscwgOF9_Ecq-Rwsq7jyQ/o.jpg',
    rating: 1.8,
    latLong: {},
    broadcasts: [{
      type: 'announcement',
      title: 'We are out of olives',
      description: 'Said no pizza man ever!'
    }]
  }, {
    id: '7',
    title: 'Welcome Hotel',
    phoneNumber: '+910000000000',
    industry: 'Hotel & Stays',
    email: 'abc@xyz.com',
    description: 'A stay you will not forget',
    address: 'Sanket Heights, Sun Pharma Rd, Parvati Nagar, Tandalja, Vadodara, Gujarat 390012',
    imgUrl: 'https://s3-media1.fl.yelpcdn.com/bphoto/U2R4guIPD-zLy7AAQyQDeA/o.jpg',
    rating: 4.7,
    latLong: {},
    broadcasts: []
  }, {
    id: '8',
    title: 'Maji Sainik',
    email: 'abc@xyz.com',
    industry: 'Food & Beverages',
    address: 'Sanket Heights, Sun Pharma Rd, Parvati Nagar, Tandalja, Vadodara, Gujarat 390012',
    description: 'Lip Smacking egg preparations that you and your friends will love!',
    imgUrl: '',
    rating: 2.7,
    latLong: {},
    broadcasts: [{
      type: 'rating',
      title: 'How was the Egg Gotala dish!',
    },  {
      type: 'alert',
      title: 'Restaurant will be closed tomorrow due to strikes.'
    }]
  }];
