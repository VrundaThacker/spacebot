import React, { useState } from 'react';
import {KeyboardAvoidingView} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import FeedbackWidget from '../components/FeedbackWidget';
import DummyChatWidget from '../components/DummyChatWidget';
import TextInputWidget from '../components/TextInputWidget';


const FeedbackScreen = () => {
  const [feedbackSent, setFeedbackSent] = useState([]);
  const handleFeedback = (ratingInput, userInputIncoming) => {
    const newFeedback = [...feedbackSent];
    newFeedback.push({
      rating: ratingInput,
      userInput: userInputIncoming
    })
    setFeedbackSent(newFeedback);
  }

  return <KeyboardAwareScrollView
    contentContainerStyle={{ flex: 1 }}
  >
    {
      feedbackSent.length
        ? <DummyChatWidget feedback={feedbackSent} />
        : <FeedbackWidget handleFeedback={handleFeedback} />
    }
    <TextInputWidget handleFeedback={handleFeedback} />
  </KeyboardAwareScrollView>
}
export default FeedbackScreen;