import React, { useEffect } from 'react';
import { StyleSheet, ImageBackground, Image } from 'react-native';
import { SafeAreaView } from 'react-navigation';


const StartScreen = ({ navigation }) => {
  useEffect(() => {
    const timeoutHandle = setTimeout(() => {
      navigation.navigate('MySpacesList')
    }, 2000);
  }, [])

  return (
    <SafeAreaView forceInset={{ top: 'always' }} style={{
      flex: 1
    }}>
      <ImageBackground
        source={require('../../assets/splash.png')}
        resizeMode={'cover'}
        style={{
          alignItems: 'center',
          paddingTop: 30,
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center'
        }}>
      </ImageBackground>
    </SafeAreaView>
  );
};

StartScreen.navigationOptions = () => {
  return {
    header: null
  };
};

const styles = StyleSheet.create({
});

export default StartScreen;