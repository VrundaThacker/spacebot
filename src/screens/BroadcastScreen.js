import React, { useState } from 'react';
import { StyleSheet, View, Text, Image, ScrollView } from 'react-native';
import { Button } from 'native-base';
import StarRating from 'react-native-star-rating';
import { Analytics } from 'aws-amplify';
import { MaterialCommunityIcons, FontAwesome5 } from '@expo/vector-icons';
import Spacer from '../components/Spacer';
import ProgressBar from '../components/ProgressBar';
import Poll from '../components/Poll';

const PollWidget = ({ poll }) => {
  const [selectedKey, setSelectedKey] = useState('');
  return (
    <View style={{
      borderRadius: 20,
      borderColor: 'rgba(128, 128, 128, 0.25)',
      borderWidth: 0.5,
      height: 340,
    }}>
      <View style={{
        flexDirection: 'row',
        alignItems: 'center'
      }}>
        <MaterialCommunityIcons name={'poll'} style={{
          fontSize: 22,
          margin: 15,
          color: '#FAAF1D',
          backgroundColor: '#FEF1D9',
          padding: 12,
          borderRadius: 12,
          overflow: 'hidden',
          width: 50,
          borderWidth: 0
        }} />
        <Text style={{

        }}>Poll</Text>
      </View>

      <ProgressBar
        height={4}
        primaryColor={'#FEF1D9'}
        borderRadius={2}
        overlapColor={'#FAAF1D'}
        overlapWidth={'20%'}
      />
      <Spacer />
      <Poll
        question={poll.title}
        options={poll.options}
        backgroundColor='#FEF1D9'
        overlapColor='rgba(250, 175, 29, 0.26)'
        selectedColor='#FAAF1D'
        textColor='#7B8993'
        onChange={(key) => {
          setSelectedKey(key);
          Analytics.record('User Responded to Poll Broadcast');
        }}
        isSelectedKey={selectedKey}
      />
      <Spacer />
      <Spacer />
    </View>
  );
}

const RatingWidget = ({ rating }) => {
  const [selectedRating, setSelectedRating] = useState(0);
  return (
    <View style={{
      borderRadius: 20,
      borderColor: 'rgba(128, 128, 128, 0.25)',
      borderWidth: 0.5,
      height: 240
    }}>
      <View style={{
        flexDirection: 'row',
        alignItems: 'center'
      }}>
        <MaterialCommunityIcons name={'star'} style={{
          fontSize: 22,
          margin: 15,
          color: '#8158F3',
          backgroundColor: '#EDE8FD',
          padding: 12,
          borderRadius: 12,
          overflow: 'hidden',
          width: 50,
          borderWidth: 0
        }} />
        <Text style={{

        }}>Rating</Text>
      </View>

      <ProgressBar
        height={4}
        primaryColor={'#EDE8FD'}
        borderRadius={2}
        overlapColor={'#8158F3'}
        overlapWidth={'95%'}
      />
      <Spacer />

      <Text style={{
        fontWeight: 'bold',
        marginLeft: 15,
        fontSize: 16
      }}>
        {rating.title}
      </Text>
      <Text style={{
        color: 'rgba(0, 0, 0, 0.2)',
        marginLeft: 15,
        fontSize: 12
      }}>{rating.description}</Text>
      <Spacer />
      <View style={{
        flexDirection: 'row',
      }}
      >
        <StarRating
          maxStars={5}
          rating={selectedRating}
          fullStarColor='#8158F3'
          emptyStarColor='#e8e8e8'
          starSize={20}
          // disabled={Boolean(selectedRating)}
          buttonStyle={{
            borderWidth: 1,
            borderRadius: 4,
            paddingHorizontal: 14,
            paddingVertical: 5,
            borderColor: '#E8E8E8',
            alignItems: 'center',
            justifyContent: 'center'
          }}
          containerStyle={{
            flex: 1,
            marginHorizontal: 20
          }}
          selectedStar={(rating) => {
            setSelectedRating(rating);
            Analytics.record('User Responded to Rating Broadcast');
          }}
        />
      </View>

    </View>
  );
}


const AnnouncementWidget = ({ announcement }) => {
  return (
    <View style={{
      borderRadius: 20,
      borderColor: 'rgba(128, 128, 128, 0.25)',
      borderWidth: 0.5,
      height: 200,
    }}>
      <View style={{
        flexDirection: 'row',
        alignItems: 'center'
      }}>
        <FontAwesome5 name={'info'} style={{
          fontSize: 22,
          margin: 15,
          color: '#EB5757',
          backgroundColor: '#FCE7E7',
          padding: 12,
          paddingLeft: 20,
          borderRadius: 12,
          overflow: 'hidden',
          width: 50,
          borderWidth: 0
        }} />
        <Text style={{

        }}>Announcement</Text>
      </View>

      <ProgressBar
        height={4}
        primaryColor={'#FCE7E7'}
        borderRadius={2}
        overlapColor={'#EB5757'}
        overlapWidth={'45%'}
      />
      <Spacer />

      <Text style={{
        fontWeight: 'bold',
        marginLeft: 15,
        fontSize: 16
      }}>
        {announcement.title}
      </Text>
      <Text style={{
        color: 'rgba(0, 0, 0, 0.2)',
        marginLeft: 15,
        fontSize: 12
      }}>{announcement.description}</Text>
      <Spacer />
      <View style={{
        flexDirection: 'row'
      }}
      >
      </View>

    </View>
  );
}

const NoBroadcastWidget = () => {
  return (
    <View style={{ alignItems: 'center' }}>
      <Image
        source={require('../../assets/noBroadcast.png')}
        style={{ height: 220 }}
      />
      <Text style={{
        color: '#ccc'
      }}>No broadcast seem to be available at this moment</Text>
      <Spacer />
      <Spacer />
      <Button style={{ backgroundColor: '#1DBDA0', paddingHorizontal: 50 }}><Text style={{ color: '#fff' }}>Refresh</Text></Button>
    </View>
  );
}

const BroadcastScreen = ({ broadcasts }) => {
  if (broadcasts.length) {
    const announcement = broadcasts.find(broadcast => broadcast.type === 'announcement');
    const rating = broadcasts.find(broadcast => broadcast.type === 'rating');
    const poll = broadcasts.find(broadcast => broadcast.type === 'poll');
    return (
      <ScrollView style={{
        flex: 1,
        marginHorizontal: 8
      }}>
        {
          announcement ? <AnnouncementWidget announcement={announcement} /> : null
        }
        {
          announcement && poll ? <Spacer /> : null
        }
        {
          poll ? <PollWidget poll={poll} /> : null
        }
        {
          rating && poll ? <Spacer /> : null
        }
        {
          rating ? <RatingWidget rating={rating} /> : null
        }
        <Spacer />
      </ScrollView>
    );
  } else {
    Analytics.record('User encountered a No Broadcast page');
    return <NoBroadcastWidget />
  }

}
export default BroadcastScreen;