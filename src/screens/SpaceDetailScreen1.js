import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Image, Text, TouchableOpacity, SafeAreaView, ScrollView, Platform, ImageBackground, Animated } from 'react-native';
import { Tabs, Tab, TabHeading, Icon, ScrollableTab, Thumbnail } from 'native-base';
import { Analytics } from 'aws-amplify';
import StarRating from 'react-native-star-rating';
import FeedbackScreen from './FeedbackScreen';
import BroadcastScreen from './BroadcastScreen';
import DetailsScreen from './DetailsScreen';
import HomeAnnouncementComponent from '../components/HomeAnnouncementComponent1';
import ReportSpaceModal from '../components/ReportSpaceModal';

const tabs = {
  0: 'Feedback',
  1: 'Broadcasts',
  2: 'Details'
};

const getHomeAnnouncementInfo = (selectedSpace) => {
  if (selectedSpace.broadcasts && selectedSpace.broadcasts.length) {
    return selectedSpace.broadcasts.find(broadcast => broadcast.type === 'announcement')
  } else {
    return null;
  }
}

const hasAlert = (space) => {
  if (space.broadcasts && space.broadcasts.length) {
    return space.broadcasts.find(broadcast => broadcast.type === 'alert')
  } else {
    return null;
  }
}


const SpaceDetailScreen = ({ navigation }) => {
  const [selectedTabIndex, setSelectedTabIndex] = useState(0);
  const selectedSpace = navigation.getParam('selectedSpace');
  const [showReportModal, setShowReportModal] = useState(false);
  const [showAnnouncement, setShowAnnouncement] = useState(true);

  const homeAnnouncement = getHomeAnnouncementInfo(selectedSpace);
  const hasAlertMsg = hasAlert(selectedSpace)

  const showAnnouncementWidget = homeAnnouncement && homeAnnouncement.title;

  useEffect(() => {
    const listener = navigation.addListener('didFocus', () => {
      Analytics.record('Space Details screen', {
        name: selectedSpace.title
      });
    });
    return () => {
      listener.remove();
    };
  }, []);

  const imageSrc = selectedSpace.imgUrl ? { uri: selectedSpace.imgUrl } : require('../../assets/NoSpaceDetail.png');

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View>
        <ImageBackground
          source={imageSrc}
          resizeMode={'cover'}
          style={{ width: '100%', height: 240, justifyContent: 'space-between' }}
        >
          {/* header bar */}
          <View style={styles.headerActions}>
            <TouchableOpacity transparent style={{
              backgroundColor: 'rgba(2, 28, 47, 0.5)',
              borderRadius: 14,
              width: 28,
              height: 28,
              justifyContent: 'center',
              alignItems: 'center',
            }}
              onPress={() => navigation.goBack()}
            >
              <Icon
                type='MaterialCommunityIcons'
                name='arrow-left'
                style={{
                  color: '#fff',
                  fontSize: 26,
                }}
              />
            </TouchableOpacity>
            <View style={{ flexDirection: 'row', width: 30, justifyContent: 'space-between' }}>
              <TouchableOpacity transparent style={{
                backgroundColor: 'rgba(2, 28, 47, 0.5)',
                borderRadius: 14,
                width: 28,
                height: 28,
                justifyContent: 'center',
                alignItems: 'center',
              }}
                onPress={() => {
                  setShowReportModal(true);
                  Analytics.record('User Clicked on Report Space Icon');
                }}
              >
                <Icon
                  type='MaterialCommunityIcons'
                  name='flag-variant-outline'
                  style={{
                    color: '#fff',
                    fontSize: 24,
                  }}
                />
              </TouchableOpacity>
            </View>
          </View>

          <View>
            <View style={showAnnouncementWidget ? styles.infoContainerWithAnnouncement : styles.infoContainer}>
              <Text style={{
                color: 'white',
                fontWeight: 'bold',
                fontSize: 16
              }}>
                {selectedSpace.title}
              </Text>
              <View style={{ flexDirection: 'row', alignItems: 'baseline' }}>
                <StarRating
                  disabled={true}
                  maxStars={5}
                  rating={selectedSpace.rating}
                  fullStarColor='#fff'
                  emptyStarColor='rgba(255, 255, 255, 0.4)'
                  starSize={14}
                />
                <Text style={{ color: '#fff', marginLeft: 5 }}>{selectedSpace.rating}</Text>
              </View>
            </View>
            <View>
              {
                showAnnouncement && showAnnouncementWidget
                  ? <HomeAnnouncementComponent details={homeAnnouncement} navigateToBroadcasts={setSelectedTabIndex} selectedTab={selectedTabIndex} toggleVisibility={setShowAnnouncement} />
                  : null
              }
            </View>
          </View>


        </ImageBackground>
      </View>

      <View style={styles.contentContainer}>
        <Tabs
          onChangeTab={(e) => {
            setSelectedTabIndex(e.i);
            Analytics.record('User Changed Tab', {
              tabName: tabs[e.i]
            });
          }}
          renderTabBar={() =>
            <ScrollableTab
              underlineStyle={{ height: 0 }}
              style={showAnnouncementWidget ? styles.tabHeadingWithAnnouncement : styles.tabHeading}
            />}
          page={selectedTabIndex}
        >
          <Tab heading={
            <TabHeading style={{
              backgroundColor: '#fff',
              flexDirection: 'column',
              marginTop: 8,
              marginBottom: 5
            }}>
              <View style={{
                alignItems: 'center',
                borderColor: selectedTabIndex === 0 ? '#1DBDA0' : '#DADADA',
                backgroundColor: selectedTabIndex === 0 ? '#ECF9F7' : '#fff',
                borderWidth: selectedTabIndex === 0 ? 0 : 1,
                borderRadius: 10,
                width: 36,
                height: 36
              }}>
                <Icon name='star' style={{
                  fontSize: 16,
                  color: selectedTabIndex === 0 ? '#1DBDA0' : '#DADADA',
                  padding: selectedTabIndex === 0 ? 10 : 8,
                  overflow: 'hidden',
                }}
                />
              </View>
              <Text style={{ color: selectedTabIndex === 0 ? '#1DBDA0' : '#DADADA', fontSize: 12 }}>Feedback</Text>
            </TabHeading>}
          >
            <FeedbackScreen />
          </Tab>
          <Tab heading={
            <TabHeading
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                marginTop: 8,
                marginBottom: 5
              }}>
              <View style={{
                alignItems: 'center',
                borderColor: selectedTabIndex === 1 ? '#1DBDA0' : '#DADADA',
                backgroundColor: selectedTabIndex === 1 ? '#ECF9F7' : '#fff',
                borderWidth: selectedTabIndex === 1 ? 0 : 1,
                borderRadius: 10,
                width: 36,
                height: 36
              }}>
                <Icon
                  type='FontAwesome'
                  name='bullhorn'
                  style={{
                    fontSize: 16,
                    color: selectedTabIndex === 1 ? '#1DBDA0' : '#DADADA',
                    padding: selectedTabIndex === 1 ? 10 : 8,
                    overflow: 'hidden',
                  }} />
              </View>

              <Text style={{ color: selectedTabIndex === 1 ? '#1DBDA0' : '#DADADA', fontSize: 12 }}>Broadcasts</Text>

            </TabHeading>}
          >
            <BroadcastScreen broadcasts={selectedSpace.broadcasts} />
          </Tab>
          <Tab heading={
            <TabHeading
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                marginTop: 8,
                marginBottom: 5
              }}>
              <View style={{
                alignItems: 'center',
                borderColor: selectedTabIndex === 2 ? '#1DBDA0' : '#DADADA',
                backgroundColor: selectedTabIndex === 2 ? '#ECF9F7' : '#fff',
                borderWidth: selectedTabIndex === 2 ? 0 : 1,
                borderRadius: 10,
                width: 36
              }}>
                <Icon
                  type='FontAwesome'
                  name='align-justify'
                  style={{
                    fontSize: 16,
                    color: selectedTabIndex === 2 ? '#1DBDA0' : '#DADADA',
                    padding: selectedTabIndex === 2 ? 10 : 8,
                    overflow: 'hidden'
                  }} />
              </View>
              <Text style={{ color: selectedTabIndex === 2 ? '#1DBDA0' : '#DADADA', fontSize: 12 }}>Details</Text>

            </TabHeading>}
          >
            <DetailsScreen
              description={selectedSpace.description}
              address={selectedSpace.address}
              phoneNumber={selectedSpace.phoneNumber}
              email={selectedSpace.email}
            />
          </Tab>
        </Tabs>
      </View>
      {
        showReportModal
          ? <ReportSpaceModal open={showReportModal} toggleModalVisibility={setShowReportModal} />
          : null
      }

    </SafeAreaView>
  );
}

SpaceDetailScreen.navigationOptions = () => {
  return {
    header: null
  };
};

const styles = StyleSheet.create({
  infoContainerWithAnnouncement: {
    height: 40,
    backgroundColor: 'rgba(2, 28, 47, 0.5)',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingTop: 5,
  },
  infoContainer: {
    height: 30,
    backgroundColor: 'rgba(2, 28, 47, 0.5)',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingTop: 5,
  },
  contentContainer: {
    backgroundColor: 'white',
    flex: 1,
  },
  tabHeadingWithAnnouncement: {
    backgroundColor: '#fff',
    height: 60,
    marginTop: 8,
    marginBottom: 5,
  },
  tabHeading: {
    backgroundColor: '#fff',
    height: 60,
    marginTop: 8,
    marginBottom: 5,
  },
  headerActions: {
    width: '100%',

    // backgroundColor: '#1DBDA0',
    paddingHorizontal: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    ...Platform.select({
      android: {
        paddingTop: 24,
        height: 54,
      },
      ios: {
        height: 36,
        // paddingTop: 12
      }
    })
  }
})

export default SpaceDetailScreen;