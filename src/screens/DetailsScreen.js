import React from 'react';
import { StyleSheet, Text, ScrollView, View, Linking, TouchableOpacity } from 'react-native';
import { Analytics } from 'aws-amplify';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Spacer from '../components/Spacer';
import Divider from '../components/Divider';


const DetailsScreen = ({ description, address, phoneNumber, email }) => {
  return (
    <View style={{ flex: 1 }}>
      <ScrollView style={{ marginHorizontal: 10 }}>
        <Spacer />
        <Text style={{ fontSize: 14 }}>About</Text>
        <Spacer />
        <Text style={{ fontSize: 12, color: '#C4C4C4' }}>{description}</Text>
        <Spacer />
        <Divider />
        <Spacer />
        <View style={{
          flexDirection: 'row',
          flex: 1,
          marginRight: 10,
          width: 320
        }}>
          <Icon
            size={24}
            color={'#c4c4c4'}
            name='map-marker'
            style={{
              margin: 5,
            }}
          />
          <Text style={{
            marginLeft: 10,
            fontSize: 12, color: '#C4C4C4'
          }}>{address}</Text>
        </View>


      </ScrollView>
      <Divider />
      <View>
        <Divider />
        <View style={{
          height: 80,
          flexDirection: 'row',
          justifyContent: 'space-evenly',
          marginTop: 10,
        }}
        >
          {
            email
              ? <TouchableOpacity
                style={{
                  height: 50,
                  width: 50,
                  borderRadius: 25,
                  backgroundColor: 'rgba(19, 128, 208, 0.12)',
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
                onPress={() => {
                  Linking.openURL(`mailto:${email}`);
                  Analytics.record('User clicked on Email Icon');
                }}
              >
                <Icon
                  size={32}
                  color={'#1380D0'}
                  name='email-outline'
                  style={{
                    margin: 5,
                  }}
                />
              </TouchableOpacity>
              : null
          }

          {
            phoneNumber
              ? <TouchableOpacity
                style={{
                  height: 50,
                  width: 50,
                  borderRadius: 25,
                  backgroundColor: 'rgba(19, 128, 208, 0.12)',
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
                onPress={() => {
                  Linking.openURL(`tel:${phoneNumber}`);
                  Analytics.record('User clicked on Call Icon');
                }}
              >
                <Icon
                  size={32}
                  color={'#1380D0'}
                  name='phone'
                  style={{
                    margin: 5,
                  }}
                />
              </TouchableOpacity>
              : null
          }

        </View>
      </View>
    </View>
  );
}
export default DetailsScreen;