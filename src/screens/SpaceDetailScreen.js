import React, { useState } from 'react';
import { View, StyleSheet, Image, Text } from 'react-native';
import { Tabs, Tab, TabHeading, Icon, ScrollableTab, Button } from 'native-base';
import StarRating from 'react-native-star-rating';
import FeedbackScreen from './FeedbackScreen';
import BroadcastScreen from './BroadcastScreen';
import DetailsScreen from './DetailsScreen';
import HomeAnnouncementComponent from '../components/HomeAnnouncementComponent';
import ReportSpaceModal from '../components/ReportSpaceModal';

const getHomeAnnouncementInfo = (selectedSpace) => {
  if (selectedSpace.broadcasts && selectedSpace.broadcasts.length) {
    return selectedSpace.broadcasts.find(broadcast => broadcast.type === 'announcement')
  } else {
    return null;
  }
}


const SpaceDetailScreen = ({ navigation }) => {
  const [selectedTabIndex, setSelectedTabIndex] = useState(0);
  const selectedSpace = navigation.getParam('selectedSpace');
  const [showReportModal, setShowReportModal] = useState(false);
  const homeAnnouncement = getHomeAnnouncementInfo(selectedSpace);

  const showAnnouncementWidget = selectedTabIndex === 0 && homeAnnouncement && homeAnnouncement.title;

  return (
    <View style={{ flex: 1, position: 'relative' }}>
      {
        selectedSpace.imgUrl
          ? <Image source={{ uri: selectedSpace.imgUrl }} style={showAnnouncementWidget ? styles.imageWithAnnouncement : styles.image} />
          : <Image source={require('../../assets/NoSpaceDetail.png')} style={showAnnouncementWidget ? styles.imageWithAnnouncement : styles.image} />
      }
      <View style={{
        width: '98%',
        position: 'absolute',
        flexDirection: 'row',
        justifyContent: 'space-between',
        top: 16,
        left: 5
      }}>
        <Button transparent style={{
          backgroundColor: 'rgba(2, 28, 47, 0.5)',
          borderRadius: 14,
          width: 28,
          height: 28,
          justifyContent: 'center'
        }}
        onPress={() => navigation.goBack()}
        >
          <Icon
            type='MaterialCommunityIcons'
            name='arrow-left'
            style={{
              color: '#fff',
              fontSize: 20,
              paddingRight: 20,
              paddingTop: -5
            }}
          />
        </Button>

        <Button transparent style={{
          backgroundColor: 'rgba(2, 28, 47, 0.5)',
          borderRadius: 14,
          width: 28,
          height: 28,
          justifyContent: 'center'
        }}
        onPress={() => setShowReportModal(true)}
        >
          <Icon
            type='MaterialCommunityIcons'
            name='flag-variant-outline'
            style={{
              color: '#fff',
              fontSize: 20,
              paddingRight: 20,
              paddingTop: -5
            }}
          />
        </Button>
      </View>
      <View style={showAnnouncementWidget ? styles.infoContainerWithAnnouncement : styles.infoContainer}>
        <Text style={{
          color: 'white',
          fontWeight: 'bold',
          fontSize: 16
        }}>
          {selectedSpace.title}
        </Text>
        <View style={{ flexDirection: 'row', alignItems: 'baseline' }}>
          <StarRating
            disabled={true}
            maxStars={5}
            rating={selectedSpace.rating}
            fullStarColor='#fff'
            emptyStarColor='rgba(255, 255, 255, 0.4)'
            starSize={14}
          />
          <Text style={{ color: '#fff', marginLeft: 5 }}>{selectedSpace.rating}</Text>
        </View>
      </View>

      <View style={showAnnouncementWidget ? styles.contentContainerWithAnnouncement : styles.contentContainer}>
        <Tabs
          onChangeTab={(e) => { setSelectedTabIndex(e.i); }}
          renderTabBar={() =>
            <ScrollableTab
              underlineStyle={{ height: 0 }}
              style={showAnnouncementWidget ? styles.tabHeadingWithAnnouncement : styles.tabHeading}
            />}
          page={selectedTabIndex}
        >
          <Tab heading={
            <TabHeading style={{
              backgroundColor: '#fff',
              flexDirection: 'column',
              marginTop: 8,
              marginBottom: 5
            }}>
              <Icon name='star' style={{
                fontSize: 16,
                color: selectedTabIndex === 0 ? '#1DBDA0' : '#DADADA',
                borderColor: selectedTabIndex === 0 ? '#1DBDA0' : '#DADADA',
                backgroundColor: selectedTabIndex === 0 ? '#ECF9F7' : '#fff',
                borderWidth: selectedTabIndex === 0 ? 0 : 1,
                padding: 8,
                borderRadius: 10,
                overflow: 'hidden',
              }}
              />
              <Text style={{ color: selectedTabIndex === 0 ? '#1DBDA0' : '#DADADA', fontSize: 12 }}>Feedback</Text>
            </TabHeading>}
          >
            <FeedbackScreen />
          </Tab>
          <Tab heading={
            <TabHeading
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                marginTop: 8,
                marginBottom: 5
              }}>
              <Icon
                type='FontAwesome'
                name='bullhorn'
                style={{
                  fontSize: 16,
                  color: selectedTabIndex === 1 ? '#1DBDA0' : '#DADADA',
                  borderColor: selectedTabIndex === 1 ? '#1DBDA0' : '#DADADA',
                  backgroundColor: selectedTabIndex === 1 ? '#ECF9F7' : '#fff',
                  borderWidth: selectedTabIndex === 1 ? 0 : 1,
                  padding: 6,
                  borderRadius: 10,
                  overflow: 'hidden'
                }} />
              <Text style={{ color: selectedTabIndex === 1 ? '#1DBDA0' : '#DADADA', fontSize: 12 }}>Broadcasts</Text>
            </TabHeading>}
          >
            <BroadcastScreen broadcasts={selectedSpace.broadcasts} />
          </Tab>
          <Tab heading={
            <TabHeading
              style={{
                backgroundColor: '#fff',
                flexDirection: 'column',
                marginTop: 8,
                marginBottom: 5
              }}>
              <Icon
                type='FontAwesome'
                name='align-justify'
                style={{
                  fontSize: 16,
                  color: selectedTabIndex === 2 ? '#1DBDA0' : '#DADADA',
                  borderColor: selectedTabIndex === 2 ? '#1DBDA0' : '#DADADA',
                  backgroundColor: selectedTabIndex === 2 ? '#ECF9F7' : '#fff',
                  borderWidth: selectedTabIndex === 2 ? 0 : 1,
                  padding: 8,
                  borderRadius: 10,
                  overflow: 'hidden'
                }} />
              <Text style={{ color: selectedTabIndex === 2 ? '#1DBDA0' : '#DADADA', fontSize: 12 }}>Details</Text>
            </TabHeading>}
          >
            <DetailsScreen
              description={selectedSpace.description}
              address={selectedSpace.address}
              phoneNumber={selectedSpace.phoneNumber}
              email={selectedSpace.email}
            />
          </Tab>
        </Tabs>
      </View>
      {
        selectedTabIndex === 0 && homeAnnouncement && homeAnnouncement.title
          ? <HomeAnnouncementComponent details={homeAnnouncement} navigateToBroadcasts={setSelectedTabIndex} />
          : null
      }
      {
        showReportModal
        ? <ReportSpaceModal open={showReportModal} toggleModalVisibility={setShowReportModal} />
        : null
      }
    </View>
  );
}

SpaceDetailScreen.navigationOptions = () => {
  return {
    header: null
  };
};

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: 180
  },
  imageWithAnnouncement: {
    width: '100%',
    height: 200
  },
  infoContainer: {
    height: 80,
    marginTop: -70,
    backgroundColor: 'rgba(2, 28, 47, 0.5)',
    flexDirection: 'row',
    alignItems: 'baseline',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingTop: 10
  },
  infoContainerWithAnnouncement: {
    height: 100,
    marginTop: -100,
    backgroundColor: 'rgba(2, 28, 47, 0.5)',
    flexDirection: 'row',
    alignItems: 'baseline',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingTop: 10
  },
  contentContainerWithAnnouncement: {
    backgroundColor: 'white',
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
    flex: 1,
    marginTop: -40,
  },
  contentContainer: {
    backgroundColor: 'white',
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
    flex: 1,
    marginTop: -40,
  },
  tabHeading: {
    backgroundColor: '#fff',
    marginBottom: 5,
    height: 60,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  tabHeadingWithAnnouncement: {
    backgroundColor: '#fff',
    height: 60,
    marginTop: 68,
    marginBottom: 5,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  }
})

export default SpaceDetailScreen;