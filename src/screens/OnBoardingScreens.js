import React from 'react';
import { View, Image, StyleSheet, Text, SafeAreaView } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';


const slides = [
  {
    key: 'explore',
    title: 'Explore Spaces',
    text: 'Easiest way to connect with spaces within 100m distance',
    image: require('../../assets/OB1.png'),
    backgroundColor: '#fff',
    imgStyle: {
      width: '100%',
      height: '45%',
    }
  },
  {
    key: 'feedback',
    title: 'Easy Feedback',
    text: 'Give anonymous and instant feedback to the space you are visiting',
    image: require('../../assets/OB2.png'),
    backgroundColor: '#fff',
    imgStyle: {
      width: '100%',
      height: '45%',
    }
  },
  {
    key: 'broadcasts',
    title: 'Receive Broadcasts',
    text: 'Stay updated with the latest broadcasts from spaces around you',
    image: require('../../assets/OB3.png'),
    backgroundColor: '#fff',
    imgStyle: {
      width: '90%',
      height: '30%',
    }
  },
  
];

const OnBoardingScreens = ({ toggleAppVisibility }) => {


  const renderItem = (slide) => {
    return (
      <View style={styles.container}>

        <Image style={slide.item.imgStyle} source={slide.item.image} resizeMode={'contain'}/>

        <View style={styles.textContainer}>
          <Text style={styles.title}>{slide.item.title}</Text>
          <Text style={styles.description}>{slide.item.text}</Text>
        </View>
      </View>
    );
  }

  const renderSkipButton = () => {
    return <Text style={styles.skip}>Skip</Text>
  }

  const onDone = () => {
    // User finished the introduction. Show real app through
    // navigation or simply by controlling state
    toggleAppVisibility(true);
  }

  return <AppIntroSlider
    renderItem={renderItem}
    slides={slides}
    onDone={onDone}
    dotStyle={styles.dotStyle}
    activeDotStyle={styles.activeDotStyle}
    showSkipButton={true}
    renderSkipButton={renderSkipButton}
    onSkip={onDone}
    showDoneButton={true}
    // renderDoneButton={renderDoneButton}
    doneLabel={'Let\'s Explore'}
    paginationStyle={styles.paginationStyle}
    bottomButton={true}
    buttonStyle={[styles.done]}
    showPrevButton={false}
    showNextButton={false}
  />
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden'
  },
  textContainer: {
    marginTop: 30,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center'
  },
  title: {
    color: '#021C2F',
    fontSize: 22,
    fontWeight: 'bold'
  },
  description: {
    color: '#979797',
    fontSize: 16,
    textAlign: 'center',
    marginTop: 10,
    marginHorizontal: 10
  },
  dotStyle: {
    backgroundColor: '#DADADA'
  },
  activeDotStyle: {
    backgroundColor: '#1DBDA0'
  },
  skip: {
    marginLeft: '90%',
    color: '#979797'
  },
  done: {
    backgroundColor: '#1DBDA0',
    borderRadius: 24,
    marginTop: 10
  },
  doneText: {
    color: '#fff'
  },
  paginationStyle: {
    marginBottom: '5%'
  }
});


export default OnBoardingScreens;