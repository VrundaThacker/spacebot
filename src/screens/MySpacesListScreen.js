import React, { useEffect, useState } from 'react';
import { View, StyleSheet, SafeAreaView, TouchableOpacity, Image } from 'react-native';
import { List, ListItem, Left, Body, Right, Thumbnail, Text, Icon } from 'native-base';
import StarRating from 'react-native-star-rating';
import { Analytics } from 'aws-amplify';
import { mySpacesList } from '../data/spaces';
import AppFeedback from '../components/AppFeedback';

const hasAlert = (space) => {
  if (space.broadcasts && space.broadcasts.length) {
    return space.broadcasts.find(broadcast => broadcast.type === 'alert')
  } else {
    return null;
  }
}

const MySpacesListScreen = ({ navigation }) => {
  const [showAppFeedBack, setAppFeedback] = useState(false);

  useEffect(() => {
    const listener = navigation.addListener('didFocus', () => {
      Analytics.record('User viewed the Spaces List Screen')
    });
    return () => {
      listener.remove();
    };
  }, [])

  const renderSpaceItem = (space) => {
    const spaceHasAlert = hasAlert(space);

    return (
      <ListItem thumbnail onPress={() => {
        navigation.navigate('SpaceDetail1', {
          selectedSpace: space
        });
        Analytics.record('User entered Space Detail');
      }
      }>
        <Left>
          <View style={{alignItems: 'center'}}>
            {
              space.imgUrl
                ? <Thumbnail square source={{ uri: space.imgUrl }} style={styles.image} />
                : <Thumbnail square source={require('../../assets/noSpacePhoto.png')} style={styles.image} />
            }
            {
              // spaceHasAlert
              //   ? <View style={{
              //     marginTop: -8,
              //     backgroundColor: '#EB5757',
              //     borderRadius: 4,
              //     width: 50,
              //     height: 16,
              //     justifyContent: 'center'
              //   }}>
              //     <Text style={{fontSize: 10, color: '#fff'}}>ALERT</Text>
              //   </View>
              //   : null
            }
          </View>
        </Left>
        <Body>
          <View style={{
            alignItems: 'flex-start'
          }}>
            <Text style={styles.name}>{space.title}</Text>
            <Text style={{ color: '#ccc', fontSize: 14, marginLeft: 5 }}>{space.industry}</Text>
            <View style={{
              flexDirection: 'row',
              alignItems: 'baseline',
              flex: 1,
              marginTop: 4
            }}>
              <StarRating
                disabled={true}
                maxStars={5}
                rating={space.rating}
                fullStarColor='#FAAF1D'
                emptyStarColor='#E8E8E8'
                starSize={14}
                containerStyle={{
                  marginLeft: 5,
                  marginTop: 5
                }}
              />
              <Text style={{ color: '#ccc', fontSize: 14, marginLeft: 10 }}>{space.rating}</Text>
            </View>
          </View>
        </Body>
        <Right>

          <Icon type='MaterialCommunityIcons' name='chevron-right' />

        </Right>
      </ListItem>
    );
  }


  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={{
        flexDirection: 'row',
        backgroundColor: '#1DBDA0',
        paddingLeft: 10,
        paddingVertical: 28,
        justifyContent: 'space-between'
      }}>
        <View>
          <Text style={{
            color: '#fff',
            fontWeight: 'bold',
            fontSize: 20
          }}>Spaces Around Me</Text>
          <Text style={{
            color: 'rgba(0, 0, 0, 0.31)',
            fontSize: 14,
            marginBottom: 10
          }}>You can see spaces within 100m radius</Text>
        </View>
        {/* <Icon type='MaterialIcons' name='more-vert' style={{color: '#fff'}}/> */}
        <TouchableOpacity
          onPress={() => {
            setAppFeedback(true);
            Analytics.record('User Opened App Feedback form');
          }
          }
        >
          <Image style={{
            height: 26,
            width: 20,
            marginRight: 10
          }} source={require('../../assets/feedbackForm.png')} />
        </TouchableOpacity>
      </View>
      <View style={styles.container}>
        <List
          dataArray={mySpacesList}
          renderRow={renderSpaceItem}
        />
      </View>
      {
        showAppFeedBack
          ? <AppFeedback open={showAppFeedBack} toggleModalVisibility={setAppFeedback} />
          : null
      }
    </SafeAreaView >
  );
};

MySpacesListScreen.navigationOptions = () => {
  return {
    header: null
  };
};

const styles = StyleSheet.create({
  detailInfocontainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 5,
    marginTop: 20
  },
  detailContainer: {
    backgroundColor: '#FFF',
    margin: 5,
    flexDirection: 'row',
    padding: 10,
    borderRadius: 9
  },
  container: {
    paddingLeft: 5,
    paddingTop: 5,
    backgroundColor: '#fff',
    flex: 1,
    borderTopLeftRadius: 24,
    borderTopRightRadius: 24,
    marginTop: -24

  },
  listContainer: {
    // backgroundColor: '#FFF',
    paddingTop: 20
  },
  icon: {
    fontSize: 180,
    color: '#1380D0'
  },
  emptyview: {
    flex: 1,
    alignItems: 'center',
    // marginTop: 80
    justifyContent: 'center'
  },
  textLarge: {
    fontSize: 20
  },
  textSmall: {
    fontSize: 14,
    color: 'rgba(0, 0, 0, 0.2)'
  },
  image: {
    borderRadius: 4,
    width: 60,
    height: 60,
  },
  name: {
    fontWeight: 'bold',
    fontSize: 16,
    marginLeft: 5
  },
});

export default MySpacesListScreen;
